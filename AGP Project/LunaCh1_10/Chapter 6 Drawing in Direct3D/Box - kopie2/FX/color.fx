cbuffer cbPerObject
{
	float iGlobalTime;
	float4x4 gWorldViewProj; 
};

struct VertexIn
{
	float3 PosL  : POSITION;
    float4 Color : COLOR;
};

struct VertexOut
{
	float4 PosH  : SV_POSITION;
    float4 Color : COLOR;
};

VertexOut VS(VertexIn vin)
{
	VertexOut vout;
	
	// Transform to homogeneous clip space.
	vout.PosH = mul(float4(vin.PosL, 1.0f), gWorldViewProj);
	
	// Just pass vertex color into the pixel shader.
    vout.Color = vin.Color;
    
    return vout;
}

float4 PS(VertexOut pin) : SV_Target
{
	float4 color;
	float2 iResolution = float2(800, 600);
	float circleX = iResolution.x / 2.0;
	float circleY = iResolution.y / 2.0;
	float deltaX = pin.PosH.x - circleX;
	float deltaY = pin.PosH.y - circleY;
	float dist = sqrt(deltaX*deltaX + deltaY*deltaY);
	float light = 0.5*cos(dist*0.5 - iGlobalTime * 80.0) + 0.5;

	color = float4(light, light, light, 1.0);
	return color;
}

technique11 ColorTech
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_5_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_5_0, PS()));
	}
}