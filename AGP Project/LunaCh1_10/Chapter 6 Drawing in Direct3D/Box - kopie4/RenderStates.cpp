//***************************************************************************************
// RenderStates.cpp by Frank Luna (C) 2011 All Rights Reserved.
//***************************************************************************************

#include "RenderStates.h"

ID3D11BlendState*      RenderStates::AlphaBlend = 0;
ID3D11BlendState*      RenderStates::AdditiveBlend = 0;
ID3D11BlendState*      RenderStates::MultiBlend = 0;
ID3D11BlendState*      RenderStates::Multi2Blend = 0;

void RenderStates::InitAll(ID3D11Device* device)
{
	//DEMO Demo demo
	// Alpha Blending
	D3D11_BLEND_DESC DescAlphaBlend = { 0 };
	DescAlphaBlend.AlphaToCoverageEnable = false;
	DescAlphaBlend.IndependentBlendEnable = false;

	DescAlphaBlend.RenderTarget[0].BlendEnable = true;
	DescAlphaBlend.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	DescAlphaBlend.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;

	DescAlphaBlend.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	DescAlphaBlend.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	DescAlphaBlend.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	DescAlphaBlend.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	DescAlphaBlend.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HR(device->CreateBlendState(&DescAlphaBlend, &AlphaBlend));

	// Additive Blending
	D3D11_BLEND_DESC DescAdditiveBlend = { 0 };
	DescAdditiveBlend.AlphaToCoverageEnable = false;
	DescAdditiveBlend.IndependentBlendEnable = false;

	DescAdditiveBlend.RenderTarget[0].BlendEnable = true;
	DescAdditiveBlend.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	DescAdditiveBlend.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;

	DescAdditiveBlend.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	DescAdditiveBlend.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	DescAdditiveBlend.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	DescAdditiveBlend.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	DescAdditiveBlend.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HR(device->CreateBlendState(&DescAdditiveBlend, &AdditiveBlend));

	// Multiplicative Blending
	D3D11_BLEND_DESC DescMultiBlend = { 0 };
	DescMultiBlend.AlphaToCoverageEnable = false;
	DescMultiBlend.IndependentBlendEnable = false;

	DescMultiBlend.RenderTarget[0].BlendEnable = true;
	DescMultiBlend.RenderTarget[0].SrcBlend = D3D11_BLEND_ZERO;
	DescMultiBlend.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_COLOR;

	DescMultiBlend.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	DescMultiBlend.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	DescMultiBlend.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	DescMultiBlend.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	DescMultiBlend.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HR(device->CreateBlendState(&DescMultiBlend, &MultiBlend));

	// Multiplicative2 Blending
	D3D11_BLEND_DESC DescMulti2Blend = { 0 };
	DescMulti2Blend.AlphaToCoverageEnable = false;
	DescMulti2Blend.IndependentBlendEnable = false;

	DescMulti2Blend.RenderTarget[0].BlendEnable = true;
	DescMulti2Blend.RenderTarget[0].SrcBlend = D3D11_BLEND_DEST_COLOR;
	DescMulti2Blend.RenderTarget[0].DestBlend = D3D11_BLEND_SRC_COLOR;

	DescMulti2Blend.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	DescMulti2Blend.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	DescMulti2Blend.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;

	DescMulti2Blend.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	DescMulti2Blend.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	HR(device->CreateBlendState(&DescMulti2Blend, &Multi2Blend));
}

void RenderStates::DestroyAll()
{
	ReleaseCOM(AlphaBlend);
	ReleaseCOM(AdditiveBlend);
	ReleaseCOM(MultiBlend);
	ReleaseCOM(Multi2Blend);
}