//***************************************************************************************
// RenderStates.h by Frank Luna (C) 2011 All Rights Reserved.
//   
// Defines render state objects.  
//***************************************************************************************

#ifndef RENDERSTATES_H
#define RENDERSTATES_H

#include "d3dUtil.h"

class RenderStates
{
public:
	static void InitAll(ID3D11Device* device);
	static void DestroyAll();

	//DEMO Demo demo
	static ID3D11BlendState* AlphaBlend;
	static ID3D11BlendState* AdditiveBlend;
	static ID3D11BlendState* MultiBlend;
	static ID3D11BlendState* Multi2Blend;
};

#endif // RENDERSTATES_H